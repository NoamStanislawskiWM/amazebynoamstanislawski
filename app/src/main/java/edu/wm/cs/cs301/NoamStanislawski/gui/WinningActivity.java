package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import edu.wm.cs.cs301.NoamStanislawski.R;

/**
 * This is the win screen.
 * It displays a win message and certain statistics
 * from your game. It shows the path length traveled,
 * and how much energy was consumed if a driver was used.
 * You can restart by pressing the back button.
 */
public class WinningActivity extends Activity {
    private int pathLength;
    private int energyConsumption;
    protected TextView pathLengthText;
    protected TextView consumptionText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winning);
        pathLength = getIntent().getIntExtra("PATH_LENGTH", 0);
        energyConsumption = getIntent().getIntExtra("ENERGY_CONSUMPTION", 0);
        pathLengthText = (TextView) findViewById(R.id.pathLengthText);
        consumptionText = (TextView) findViewById(R.id.consumptionText);

        //  make sure path length at this point is legit.
        CharSequence pathHolder = String.valueOf("You traveled: " + pathLength + " steps!");
        pathLengthText.setText(pathHolder);

        CharSequence energyHolder = String.valueOf("You consumed: " + energyConsumption);
        consumptionText.setText(energyHolder);


    }

    public void onBackPressed() {
        startActivity(new Intent(WinningActivity.this, AMazeActivity.class));
        this.finish();
    }
}
