package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.fonts.Font;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

public class MazePanel extends View implements P5Panel {

    private Paint panelPaint = new Paint();
    private int VIEW_WIDTH = 870;
    private int VIEW_HEIGHT = 870;
    private Font markerFont;
    private Color color;

    // These are the colors used by the maze, represented in their rgb integer value.
    // highlight and shading colors cannot be integers as they contain alpha values, so they remain Color objects.
    static final int MARKER_COLOR = 0;

    static final Color CIRCLE_HIGHLIGHT = Color.valueOf(1.0f, 1.0f, 1.0f, 0.8f);
    static final Color CIRCLE_SHADE = Color.valueOf(1.0f, 1.0f, 1.0f, 0.3f); //new Color(0.0f, 0.0f, 0.0f, 0.2f);
    static final Color  greenWM = Color.valueOf(17, 87, 64, 1);
    static final Color  goldWM = Color.valueOf(145, 111, 65, 1);
    static final Color yellowWM = Color.valueOf(255, 255, 153, 1);
    static final Color lightGray = Color.valueOf(211, 211, 211, 1);
    static final Color yellow = Color.valueOf(16776960);
    static final Color gray = Color.valueOf(8421504);
    static final Color white = Color.valueOf(16777215);
    static final Color red = Color.valueOf(16711680);

    Path polyPath = new Path();
    Bitmap noteBitmap = Bitmap.createBitmap(870, 870, Bitmap.Config.ARGB_8888);;
    Canvas noteCanvas = new Canvas(noteBitmap);
    DisplayMetrics dm;

    public MazePanel(Context context) {
        super(context);
    }

    public MazePanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MazePanel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }



    //    /**
//     * Draws the buffer image to the given graphics object.
//     * This method is called when this panel should redraw itself.
//     * The given graphics object is the one that actually shows
//     * on the screen.
//     */
//    @Override
//    public void paint(Graphics g) {
//        if (null == g) {
//            System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
//        }
//        else {
//            g.drawImage(bufferImage,0,0,null);
//        }
//    }
    @Override
    protected void onDraw(Canvas canvas) {
        if(canvas == null) {
            Log.v("MazePanel.onDraw","no canvas object, skipping drawing operation.");
        }
        else {
            canvas.drawBitmap(noteBitmap,50,100, panelPaint);
        }
    }


//    /**
//     * Obtains a graphics object that can be used for drawing.
//     * This MazePanel object internally stores the graphics object
//     * and will return the same graphics object over multiple method calls.
//     * The graphics object acts like a notepad where all clients draw
//     * on to store their contribution to the overall image that is to be
//     * delivered later.
//     * To make the drawing visible on screen, one needs to trigger
//     * a call of the paint method, which happens
//     * when calling the update method.
//     * @return graphics object to draw on, null if impossible to obtain image
//     */
//    public Graphics getBufferGraphics() {
//        // if necessary instantiate and store a graphics object for later use
//        if (!isOperational()) {
//            if (null == bufferImage) {
//                bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
//                if (null == bufferImage)
//                {
//                    System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
//                    return null; // still no buffer image, give up
//                }
//            }
//            graphics = (Graphics2D) bufferImage.getGraphics();
//            if (null == graphics) {
//                System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
//            }
//        }
//        else {
//            // For drawing in FirstPersonDrawer, setting rendering hint
//            // became necessary when lines of polygons
//            // that were not horizontal or vertical looked ragged
//            graphics.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
//            graphics.setRenderingHint(java.awt.RenderingHints.KEY_INTERPOLATION, java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        }
//
//        return graphics;
//    }
//
//
//    @Override
//    public void commit() {
//        paint(getGraphics());
//
//    }
    @Override
    public void commit() {
        draw(noteCanvas);
    }

    @Override
    public boolean isOperational() {
        return false;
    }
//
//    public void commit(Graphics g)  {
//        paint(g);
//    }


//    @Override
//    public boolean isOperational() {
//        if (null == graphics) {
//
//            if (null == bufferImage) {
//                bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
//                if (null == bufferImage)
//                {
//                    System.out.println("NULL IMAGE REMAINS NULL");
//                    return false; // still no buffer image, give up
//                }
//            }
//
//            graphics = (Graphics2D) bufferImage.getGraphics();
//            if (null == graphics) {
//                System.out.println("NULL GRAPHICS REMANINS FAILED");
//                return false;
//            }
//        }
//        return true;
//    }
//
    @Override
    public void setColor(int rgb) {
        color = Color.valueOf(rgb);
        panelPaint.setColor(color.toArgb());

    }

    public void setColor(Color c) {
        color = c;
        panelPaint.setColor(color.toArgb());
    }
//
//    public Font getFont() {
//        return markerFont;
//    }

    public Font getFont() {
        return markerFont;
    }
//
//    public void setFont(Font f) {
//        markerFont = f;
//    }
//
    public void setFont(Font f) {
        markerFont = f;
    }

    public int getColor() {
        return color.toArgb();
    }

    public float getRed() {
        return color.red();
    }

    public float getGreen() {
        return color.green();

    }

    public float getBlue() {
        return color.blue();
    }

    @Override
    public int getWallColor(int distance, int cc, int extensionX) {
        distance /= 4;
        final int part1 = distance & 7;
        final int add = (extensionX != 0) ? 1 : 0;
        final int rgbValue = ((part1 + 2 + add) * 70) / 8 + 80;
        return rgbValue;
    }




    /**
     * Draws two solid rectangles to provide a background.
     * Note that this also erases any previous drawings.
     * The color setting adjusts to the distance to the exit to
     * provide an additional clue for the user.
     * Colors transition from black to gold and from grey to green.
     * Substitute for FirstPersonView.drawBackground method.
     * @param percentToExit gives the distance to exit
     */
    @Override
    public void addBackground(float percentToExit) {

        setColor(getBackgroundColor(percentToExit, true).toArgb());
        addFilledRectangle(0, 0, VIEW_WIDTH, VIEW_HEIGHT/2);

        // grey rectangle in lower half of screen
        // graphics.setColor(Color.darkGray);
        // dynamic color setting:
        setColor(getBackgroundColor(percentToExit, false).toArgb());
        addFilledRectangle(0, VIEW_HEIGHT/2, VIEW_WIDTH, VIEW_HEIGHT);

    }
//
//    void setColor(Color c) {
//        color = c;
//        graphics.setColor(color);
//
//    }


    /**
     * Determine the background color for the top and bottom
     * rectangle as a blend between starting color settings
     * of black and grey towards gold and green as final
     * color settings close to the exit
     * @param percentToExit
     * @param top is true for the top triangle, false for the bottom
     * @return the color to use for the background rectangle
     */
    private Color getBackgroundColor(float percentToExit, boolean top) {
        Color bg_color = top? blend( yellowWM, goldWM, percentToExit) : blend(lightGray, greenWM, percentToExit);
        Log.v("BackgroundColorBlended","Given color is: " + bg_color.toString());
        return bg_color;
    }

    /**
     * Calculates the weighted average of the two given colors
     * @param c0 the one color
     * @param c1 the other color
     * @param weight0 of c0
     * @return blend of colors c0 and c1 as weighted average
     */
    private Color blend(Color c0, Color c1, float weight0) {
        if (weight0 < 0.1f)
            return c1;
        if (weight0 > 0.95f)
            return c0;
        float r = (weight0 * c0.red() + (1-weight0) * c1.red());
        float g = (weight0 * c0.green() + (1-weight0) * c1.green());
        float b = (weight0 * c0.blue() + (1-weight0) * c1.blue());
        float a = Math.max(c0.alpha(), c1.alpha());

        Color toReturn = Color.valueOf(r,g,b,a);
        return toReturn;
    }


    public void addFilledRectangle(int x, int y, int width, int height) {
        noteCanvas.drawRect(x, y, width, height, panelPaint);

    }
//
//    @Override
//    public void addFilledPolygon(int[] xPoints, int[] yPoints, int nPoints) {
//        graphics.fillPolygon(xPoints, yPoints, nPoints);
//
//    }
    @Override
    public void addFilledPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        panelPaint.setStyle(Paint.Style.FILL);
        polyPath.reset(); // only needed when reusing this path for a new build
        polyPath.moveTo(xPoints[0],yPoints[0]);
        for(int i = 1; i <= nPoints; i++) {
            polyPath.lineTo(xPoints[i], yPoints[i]);
        }
        polyPath.lineTo(xPoints[0],yPoints[0]);
        noteCanvas.drawPath(polyPath, panelPaint);
    }
//
//    @Override
//    public void addPolygon(int[] xPoints, int[] yPoints, int nPoints) {
//        if(isOperational()) {
//            graphics.drawPolygon(xPoints, yPoints, nPoints);
//        }
//    }
    public void addPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        panelPaint.setStyle(Paint.Style.STROKE);
        polyPath.reset(); // only needed when reusing this path for a new build
        polyPath.moveTo(xPoints[0],yPoints[0]);
        for(int i = 1; i <= nPoints; i++) {
            polyPath.lineTo(xPoints[i], yPoints[i]);
        }
        polyPath.lineTo(xPoints[0],yPoints[0]);
        noteCanvas.drawPath(polyPath, panelPaint);
    }
//
//    @Override
//    public void addLine(int startX, int startY, int endX, int endY) {
//
//        if(isOperational()) {
//            graphics.drawLine(startX, startY, endX, endY);
//        }
//
//        else {
//            bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
//            graphics = (Graphics2D) bufferImage.getGraphics();
//            graphics.drawLine(startX, startY, endX, endY);
//        }
//
//    }
    public void addLine(int startX, int startY, int endX, int endY) {
        noteCanvas.drawLine(startX,startY,endX,endY, panelPaint);
    }
//
//    @Override
//    public void addFilledOval(int x, int y, int width, int height) {
//        if(isOperational()) {
//            graphics.fillOval(x, y, width, height);
//        }
//
//        else {
//            bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
//            graphics = (Graphics2D) bufferImage.getGraphics();
//            graphics.fillOval(x, y, width, height);
//        }
//
//    }
    public void addFilledOval(int x, int y, int width, int height) {
        panelPaint.setStyle(Paint.Style.FILL);
        noteCanvas.drawOval(x,y,x+width,y+height,panelPaint);
    }
//
//    @Override
//    public void addArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
//        if(isOperational()) {
//            graphics.drawArc(x, y, width, height, startAngle, arcAngle);
//        }
//
//        else {
//            bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
//            graphics = (Graphics2D) bufferImage.getGraphics();
//            graphics.drawArc(x, y, width, height, startAngle, arcAngle);
//        }
//
//    }
    @Override
    public void addArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
        noteCanvas.drawArc(x,y,x+width,y+height,startAngle,arcAngle,false, panelPaint);
    }
//    @Override
//    public void addMarker(float x, float y, String str) {
//        GlyphVector gv = markerFont.createGlyphVector(graphics.getFontRenderContext(), str);
//        Rectangle2D rect = gv.getVisualBounds();
//
//        x -= rect.getWidth() / 2;
//        y += rect.getHeight() / 2;
//
//        graphics.drawGlyphVector(gv, x, y);
//
//    }
    @Override
    public void addMarker(float x, float y, String str) {
        panelPaint.setTextSize(80f);
        noteCanvas.drawText(str,x,y,panelPaint);
    }

    @Override
    public void setRenderingHint(RenderingHints hintKey, RenderingHints hintValue) {

    }


}
