package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import edu.wm.cs.cs301.NoamStanislawski.R;

/**
 * This is the losing screen.
 * It displays after a driver is unable to complete a maze,
 * either due to the robot running out of energy or the driver
 * making an illegal move. It displays either a tired robot image
 * if all energy was consumed, or a broken car image if the driver has
 * crashed. It also displays statistics similar to the win screen,
 * such as path length and energy consumed, but it also includes the
 * possible shortest path to the exit.
 * Restart the game with the back button.
 */
public class LosingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);
    }

    public void onBackPressed() {
        startActivity(new Intent(LosingActivity.this, AMazeActivity.class));
        this.finish();
    }
}
