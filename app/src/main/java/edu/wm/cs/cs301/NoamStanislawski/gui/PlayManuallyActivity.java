package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import edu.wm.cs.cs301.NoamStanislawski.R;


public class PlayManuallyActivity extends Activity {

    protected ToggleButton mapToggle;
    protected ToggleButton solutionToggle;
    protected ToggleButton visibleWallToggle;
    protected SeekBar zoomBar;
    protected TextView zoomText;
    protected ImageButton upButton;
    protected ImageButton downButton;
    protected ImageButton leftButton;
    protected ImageButton rightButton;
    protected MazePanel panel;

    private int zoomLevel = 2; // DEFAULT IS MIDDLE VALUE
    private int pathLength = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing_manually);
        mapToggle = (ToggleButton) findViewById(R.id.mapToggle);
        solutionToggle = (ToggleButton) findViewById(R.id.solutionToggle);
        visibleWallToggle = (ToggleButton) findViewById(R.id.visibleWallToggle);
        zoomBar = (SeekBar) findViewById(R.id.zoomBar);
        zoomText = (TextView) findViewById(R.id.zoomText);
        upButton = (ImageButton) findViewById(R.id.upButton);
        downButton = (ImageButton) findViewById(R.id.downButton);
        leftButton = (ImageButton) findViewById(R.id.leftButton);
        rightButton = (ImageButton) findViewById(R.id.rightButton);
        panel = new MazePanel(this);

        // zoomBar settings
        zoomBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress == 0) {
                    zoomLevel = 25; // multiplication by 0 doesn't work.
                }
                else {
                    zoomLevel = progress*50;
                }

                CharSequence zoomHolder = String.valueOf(zoomLevel);
                zoomText.setText("Zoom: " + zoomHolder + "%");
                Log.v("zoomLevel","Zoom set to: " + zoomLevel + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        // mapToggle settings
        mapToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean mapSetting = mapToggle.isChecked();
                if(mapSetting) {
                    Log.v("mapToggle","Map toggled on.");
                }
                else {
                    Log.v("mapToggle","Map toggled off.");
                }
            }

        });

        // solutionToggle settings
        solutionToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean mapSetting = solutionToggle.isChecked();
                if(mapSetting) {
                    Log.v("solutionToggle","Solution toggled on.");
                }
                else {
                    Log.v("solutionToggle","Solution toggled off.");
                }
            }

        });

        // visibleWallToggle settings
        visibleWallToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean mapSetting = visibleWallToggle.isChecked();
                if(mapSetting) {
                    Log.v("visibleWallToggle","Visible walls toggled on.");
                }
                else {
                    Log.v("mapToggle","Visible Walls toggled off.");
                }
            }

        });



        // upButton settings
        upButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //MOVE FORWARD
                pathLength++;
                Log.v("manualMovement","Moved forward.");
            }
        });

        // downButton settings
        downButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //MOVE BACKWARD
                Log.v("manualMovement","Moved backward.");
            }
        });

        // leftButton settings
        leftButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //TURN LEFT
                Log.v("manualMovement","Turned left.");
            }
        });

        // rightButton settings
        rightButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //TURN RIGHT
                Log.v("manualMovement","Turned right.");
            }
        });
    }

    public void onBackPressed() {
        startActivity(new Intent(PlayManuallyActivity.this, AMazeActivity.class));
    }

}
