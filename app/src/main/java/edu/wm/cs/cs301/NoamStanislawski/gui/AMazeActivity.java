package edu.wm.cs.cs301.NoamStanislawski.gui;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Button;
import android.view.View;
import android.content.Intent;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import edu.wm.cs.cs301.NoamStanislawski.R;
import edu.wm.cs.cs301.NoamStanislawski.generation.Maze;

/**
 * This class is the title screen. It is where every user starts the game.
 * You are able to choose the type of algorithm for maze generation you desire,
 * whether that be DFS, Eller's, or Prim's. In addition, you can specify
 * if the maze has rooms or not with a switch. Finally, can also choose the level of
 * difficulty for said maze, and then switch to the screen for maze generation (GeneratingActivity.java).
 *
 * If the revisit button is pressed, a previously generated maze  will be used (assuming one exists.)
 * If the explore butotn is pressed, a new maze will be generated with a new seed.
 */
public class AMazeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    protected SeekBar skillLevelBar;
    protected Button exploreButton;
    protected Button revisitButton;
    protected Spinner algorithmSpinner;
    protected Switch roomSwitch;
    protected TextView levelText;
    protected Maze mazeConfig;


    // INFO TO SEND TO GENERATION
    protected boolean useRooms = false;
    protected int skillLevel = 0;
    protected String algorithmChoice = "DFS";
    protected int seed = 0;


    private List<String> algorithms;


    /**
     * This is the main method, runs upon creation.
     * Set up of widgets and text occurs.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MazeApp app = (MazeApp) getApplicationContext();
        mazeConfig = app.getMazeConfig();

        // settings fields up
        skillLevelBar = (SeekBar) findViewById(R.id.skillLevelBar);
        revisitButton = (Button) findViewById(R.id.revisitButton);
        exploreButton = (Button) findViewById(R.id.exploreButton);
        algorithmSpinner = (Spinner) findViewById(R.id.algorithmSpinner);
        roomSwitch = (Switch) findViewById(R.id.roomSwitch);
        levelText = (TextView) findViewById(R.id.levelText);


        /**
         * skillLevelBar settings: anytime the skill level is set,
         * the skillLevel variable is set to the value in this bar.
         */
        skillLevelBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                skillLevel = progress;
                CharSequence levelHolder = String.valueOf(progress);
                levelText.setText(levelHolder);
                Log.v("skillLevel","Level set to: " + levelHolder);
                Toast.makeText(getApplicationContext(), "Level: "+ levelHolder, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        // exploreButton settings
        // switches to GeneratingActivity when clicked, will use new seed.
        exploreButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent generationIntent = new Intent(view.getContext(), GeneratingActivity.class);
                generationIntent.putExtra("level",skillLevel);
                generationIntent.putExtra("algorithm",algorithmChoice);
                generationIntent.putExtra("useRooms",useRooms);
                startActivity(generationIntent);
            }

        });

        // revisitButton settings
        // reuse old settings for generation.
        revisitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent reusedIntent = new Intent(view.getContext(), GeneratingActivity.class);
                reusedIntent.putExtra("level",skillLevel);
                reusedIntent.putExtra("algorithm",algorithmChoice);
                reusedIntent.putExtra("useRooms",useRooms);
                reusedIntent.putExtra("reusedSeed",seed);
                startActivity(reusedIntent);
            }

        });

        // algorithmSpinner settings
        // set up algorithm choices and assignment variables
        algorithmSpinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        algorithms = new ArrayList<String>();
        algorithms.add("DFS");
        algorithms.add("Prim's");
        algorithms.add("Eller's");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, algorithms);
        algorithmSpinner.setAdapter(adapter);


        // roomSwitch settings
        roomSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roomSwitch.isChecked()) {
                    useRooms = true;
                } else {
                    useRooms = false;
                }
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        int item = algorithmSpinner.getSelectedItemPosition();
        String toDisplay = algorithms.get(item);
        algorithmChoice = toDisplay;
        Log.v("algorithmChosen","Algorithm Set: " + toDisplay);
        Toast.makeText(getApplicationContext(), toDisplay, Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
    }
}