package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.app.Application;

import edu.wm.cs.cs301.NoamStanislawski.generation.Maze;

public class MazeApp extends Application {
    private Maze mazeConfig;
    public Maze getMazeConfig() {return mazeConfig;}
    public void setMazeConfig(Maze mazeConfig) {this.mazeConfig = mazeConfig;}
}
