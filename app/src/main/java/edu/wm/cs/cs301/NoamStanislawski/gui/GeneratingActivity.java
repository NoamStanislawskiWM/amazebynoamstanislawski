package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.wm.cs.cs301.NoamStanislawski.R;
import edu.wm.cs.cs301.NoamStanislawski.generation.Maze;
import edu.wm.cs.cs301.NoamStanislawski.generation.MazeFactory;
import edu.wm.cs.cs301.NoamStanislawski.generation.Order;

/**
 * This is the generating activity.
 * You can set the driver type on this screen, along with
 * the different levels of sensor functionality.
 * A background thread generates the maze while you change
 * those settings. Once a driver has been chosen and the
 * maze has been generated, it switches to the appropriate
 * playing activity.
 */
public class GeneratingActivity extends Activity implements AdapterView.OnItemSelectedListener, Order {

    protected ProgressBar generationProgress;
    protected Spinner driverSpinner;
    protected Spinner sensorSpinner;
    protected String driverChoice;
    protected String sensorChoice;

    private int driverClickCount = 0;
    private int sensorClickCount = 0;
    private boolean driverChosen = false;
    private boolean mazeLoaded = false;
    private List<String> drivers;
    private List<String> sensors;

    private int level;
    private Builder builder;
    private String algorithm;
    private boolean isPerfect;
    private int seed;
    private Maze mazeConfig;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);
        generationProgress = (ProgressBar) findViewById(R.id.generationProgress);
        driverSpinner = (Spinner) findViewById(R.id.driverSpinner);
        sensorSpinner = (Spinner) findViewById(R.id.sensorSpinner);
        final MazeFactory factory = new MazeFactory();

        Bundle bundle = getIntent().getExtras();

        // determining seed
        if(getIntent().hasExtra("reusedSeed")) {
            seed = bundle.getInt("reusedSeed");
        }
        else {
            Random random = new Random();
            seed = random.nextInt(9999999);
        }

        level = bundle.getInt("level");
        algorithm = bundle.getString("algorithm");
        isPerfect =  !bundle.getBoolean("useRooms");

        if(algorithm.equals("DFS")) {
            builder = Order.Builder.DFS;
        }
        else if(algorithm.equals("Prim's")) {
            builder = Order.Builder.Prim;
        }
        else if(algorithm.equals("Eller's")) {
            builder = Order.Builder.Eller;
        }


        //driverSpinner settings
        driverSpinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        drivers = new ArrayList<String>();
        drivers.add("Manual");
        drivers.add("Wall Follower");
        drivers.add("Wizard");

        ArrayAdapter<String> driverAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, drivers);
        driverSpinner.setAdapter(driverAdapter);
        driverSpinner.setSelection(0); // this avoids error of no driver being selected before maze is generated.

        //sensorSpinner settings
        sensorSpinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        sensors = new ArrayList<String>();
        sensors.add("Premium");
        sensors.add("Mediocre");
        sensors.add("Soso");
        sensors.add("Shaky");

        ArrayAdapter<String> sensorAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sensors);
        sensorSpinner.setAdapter(sensorAdapter);
        sensorSpinner.setSelection(0); // this avoids error of no driver being selected before maze is generated.

        final GeneratingActivity genActiv = this;

        new Thread(new Runnable() {
            @Override
            public void run() {
                factory.order(genActiv);
                deliver(mazeConfig);
            }
        }).start();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.driverSpinner:
                driverClickCount++;
                driverChoice = driverSpinner.getSelectedItem().toString();
                int item = driverSpinner.getSelectedItemPosition();
                String toDisplay = drivers.get(item);
                Log.v("driverChosen", "driver " + driverChoice + " has been selected.");
                if (driverClickCount > 1) {
                    Toast.makeText(getApplicationContext(), toDisplay, Toast.LENGTH_SHORT).show();
                    driverChosen = true;
                }
                break;

            case R.id.sensorSpinner:
                sensorClickCount++;
                sensorChoice = sensorSpinner.getSelectedItem().toString();
                item = sensorSpinner.getSelectedItemPosition();
                toDisplay = sensors.get(item);
                if(sensorClickCount > 1)
                    Toast.makeText(getApplicationContext(), toDisplay, Toast.LENGTH_SHORT).show();
                Log.v("sensorsChosen", "sensor mode " + sensorChoice + " has been selected.");
                break;
        }
    }

    public void onNothingSelected(AdapterView<?>arg0) {
        driverChoice = driverSpinner.getSelectedItem().toString();
        sensorChoice = sensorSpinner.getSelectedItem().toString();
    }

    @Override
    public int getSkillLevel() {
        return level;
    }

    @Override
    public Builder getBuilder() {
        return builder;
    }

    @Override
    public boolean isPerfect() {
        return isPerfect;
    }

    @Override
    public int getSeed() {
        return seed;
    }

    @Override
    public void deliver(Maze mazeConfig) {
        while(generationProgress.getProgress() < 100) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if(driverChoice.equals("Manual")) {
            Log.v("ManualGameOn","Now playing game in manual mode.");
            MazeApp app = (MazeApp) getApplicationContext();
            app.setMazeConfig(mazeConfig);
            Intent manualIntent = new Intent(GeneratingActivity.this, PlayManuallyActivity.class);
            startActivity(manualIntent);
        }
        else {
            Log.v("AnimatedGameOn","Now watching game animation.");
            startActivity(new Intent(GeneratingActivity.this, PlayAnimationActivity.class));
        }
    }

    @Override
    public void updateProgress(int percentage) {
        generationProgress.setProgress(percentage);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
