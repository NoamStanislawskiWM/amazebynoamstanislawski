package edu.wm.cs.cs301.NoamStanislawski.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.Switch;

import edu.wm.cs.cs301.NoamStanislawski.R;


/**
 * This is the screen for a maze driver animation.
 * There are multiple settings one can employ:
 * You can toggle the map and its zoom like the manual play activity.
 * You can start the driver and pause it at any point.
 * You can change the speed of the driver animation.
 * You can also see which sensors are functioning.
 *
 * Currently has shortcut to win and lose screens.
 */
public class PlayAnimationActivity extends Activity {

    protected SeekBar zoomBar;
    protected TextView zoomText;
    protected ToggleButton mapToggle;
    protected Switch driverSwitch;
    protected MazePanel panel;

    private int zoomLevel = 2;
    private int pathLength = 0;
    private boolean driverRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing_animation);

        zoomBar = (SeekBar) findViewById(R.id.zoomBar);
        zoomText = (TextView) findViewById(R.id.zoomText);
        mapToggle = (ToggleButton) findViewById(R.id.mapToggle);
        driverSwitch = (Switch) findViewById(R.id.driverSwitch);
        panel = new MazePanel(this);


        // zoomBar settings --> Same as PlayManuallyActivity zoomBar
        zoomBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress == 0) {
                    zoomLevel = 25; // multiplication by 0 doesn't work.
                }
                else {
                    zoomLevel = progress*50;
                }

                CharSequence zoomHolder = String.valueOf(zoomLevel);
                zoomText.setText("Zoom: " + zoomHolder + "%");
                Log.v("zoomLevel","Zoom set to: " + zoomLevel + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        // mapToggle settings --> same as PlayManuallyActivity mapToggle.
        mapToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean mapSetting = mapToggle.isChecked();
                if(mapSetting) {
                    Log.v("mapToggle","Map toggled on.");
                }
                else {
                    Log.v("mapToggle","Map toggled off.");
                }
            }

        });

        // driverSwitch settings
        driverSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String driverMode;
                if (driverSwitch.isChecked()) {
                    driverRunning = true;
                    driverMode = "running.";
                } else {
                    driverRunning = false;
                    driverMode = "paused.";
                }
                Log.v("driverSwitch","Driver is " + driverMode);
            }
        });


    }

    public void onBackPressed() {
        startActivity(new Intent(PlayAnimationActivity.this, AMazeActivity.class));
        this.finish();
    }
}
